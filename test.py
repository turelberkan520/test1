from jotform import *

def main():

    jotformAPIClient = JotformAPIClient('YOUR API KEY')

    forms = jotformAPIClient.get_forms()

    for form in forms:
    	print(form["title"])

if __name__ == "__main__":
    main()
